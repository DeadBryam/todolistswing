/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllador;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import modelo.ToDoModelo;

/**
 *
 * @author deadbryam
 */
public class ToDoControllador {

    Conexion conexion = new Conexion();
    DefaultListModel modelo;
    ResultSet rst;

    public ToDoControllador() {
        modelo = new DefaultListModel();
    }

    public void agregarNuevo(String mensaje) {
        conexion.insertar(String.format("INSERT INTO todo VALUES (null, \"%s\");", mensaje));
    }

    public void eliminarPorId(int id) {
        conexion.eliminar(String.format("DELETE FROM todo WHERE id=%d;", id));
    }

    public void editarPorId(String mensaje, int id) {
        conexion.editar(String.format("UPDATE todo SET mensaje = \"%s\" WHERE id=%d;", mensaje, id));
    }
    
    public boolean busquedaPorMensaje(String mensaje){
        rst = conexion.obtener(String.format("SELECT COUNT(*) FROM todo WHERE mensaje = \"%s\"", mensaje));
        boolean resultado = false;
        try {
            rst.next();
            resultado = rst.getInt(1) != 0;
        } catch (SQLException ex) {
            Logger.getLogger(ToDoControllador.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return resultado;
    }

    public DefaultListModel obtenerValores() {
        modelo.clear();
        rst = conexion.obtener("SELECT * FROM todo");
        try {
            while (rst.next()) {
                modelo.addElement(new ToDoModelo(rst.getInt("id"), rst.getString("mensaje")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ToDoControllador.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return modelo;
    }
}
